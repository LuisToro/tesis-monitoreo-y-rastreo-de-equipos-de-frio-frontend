import React, { Component } from 'react';
import { Label, Table, Loader, Dimmer } from 'semantic-ui-react';
import '../../public/style/Table.css';
import DeleteEmployee from './deleteEmployee';
import EmployeeEditModal from './employeeEditModal'

export class EmployeesTable extends Component {
  constructor() {
    super();
    this.state = {
      api: [],
      foundRows: Array(0),
      mensajeDeEstado: "",
      mostrarMensajeDeEstado: false,
      open: false
    }
  }

  render() {
    return (
      <div>
        <div className="tabla">
          <Table celled className="tarjeta-tabla">
            <Table.Header>
              <Table.Row >
                <Table.HeaderCell className="cabeceras-tabla">Nombre</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Email</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Acción</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {this.props.employees !== undefined ?
                <>
                  {this.props.employees.map((value) => (
                    <Table.Row key={value.id} >

                      <Table.Cell className="bordes-tabla">
                        <Label className="nombre">{value.nombre}</Label><br></br>
                      </Table.Cell >

                      <Table.Cell className="bordes-tabla">
                        <Label>• {value.email}</Label>
                      </Table.Cell>

                      <Table.Cell colSpan="3" className="bordes-tabla">
                        <DeleteEmployee employeeId={value.id} employeeEmail={value.email} companyId={this.props.companyId} />
                        <EmployeeEditModal employee={value} />
                      </Table.Cell>
                    </Table.Row>
                  ))}</>
                :
                <div  className="no-users-message">
                  <Dimmer active inverted>
                    <Loader inverted>Ups no tienes clientes registrados, porfavor registra un cliente...</Loader>
                  </Dimmer>
                </div>

              }
            </Table.Body>

            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan='4' className="no-border">
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>

        </div>
      </div >)

  }

}
export default EmployeesTable