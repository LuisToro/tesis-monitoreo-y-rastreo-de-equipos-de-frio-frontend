import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon } from 'semantic-ui-react';
import EditEmployeePassowrd from './editEmployeePassword'

export class EditEmployeePasswordModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
        this.openModal = this.openModal.bind(this);
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                size="tiny"
                closeIcon
                trigger={
                    <Button className="register-button">
                        <Icon name="edit outline"></Icon>
                        <label className="icon-text">Editar clave</label>
                    </Button>}
            >
                <Fragment>
                    <Modal.Header>
                        <Grid columns='equal'>
                            <Grid.Column width={11}>
                            <h1>Cambiar clave secreta</h1>
                            </Grid.Column>
                        </Grid>
                    </Modal.Header>

                    <Modal.Content>
                        <Grid columns='equal'>
                            <Grid.Row>
                                <Grid.Column >
                                    <EditEmployeePassowrd employeeEmail={this.props.email} closeModal={this.openModal} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Content>
                </Fragment>
            </Modal>
        )
    }
}

export default EditEmployeePasswordModal