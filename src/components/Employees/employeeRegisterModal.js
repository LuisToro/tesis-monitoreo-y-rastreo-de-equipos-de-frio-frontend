import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon } from 'semantic-ui-react';
import RegisterEmployee from './registerEmployee'

export class RegisterEmployeeModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
        this.openModal = this.openModal.bind(this);
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                size="tiny"
                closeIcon
                trigger={
                    <Button>
                        <Icon name="add user"></Icon>
                        <label className="icon-text">Registrar Empleado</label>
                    </Button>}
            >
                <Fragment>
                    <Modal.Header>
                        <Grid columns='equal'>
                            <Grid.Column width={11}>
                                <h1>Registra a un empleado</h1>
                            </Grid.Column>
                        </Grid>
                    </Modal.Header>

                    <Modal.Content>
                        <Grid columns='equal'>
                            <Grid.Row>
                                <Grid.Column >
                                    <RegisterEmployee companyId={this.props.companyId} closeModal={this.openModal} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Content>
                </Fragment>
            </Modal>
        )
    }
}

export default RegisterEmployeeModal