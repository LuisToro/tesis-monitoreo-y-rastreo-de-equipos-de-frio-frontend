import React from "react";
import { Button, Confirm } from "semantic-ui-react";
import axios from "axios";

const deleteEmployee = `${process.env.REACT_APP_CLIENTS_local}employees/`;

function Eliminar({ employeeId, employeeEmail, companyId }) {
    const [abierto, setOpen] = React.useState(false);

    const onOpen = () => setOpen(true);
    const onClose = () => setOpen(false);

    const deleteEmployees = (employeeId, employeeEmail, companyId) => {
        axios
            .delete(`${deleteEmployee}${employeeId}/email/${employeeEmail}/company/${companyId}`)
            .then(response => {
                window.open("/", "_self");
            })
            .catch(function (error) {
                console.log(error);
            });
        onClose();
    }

    return (
        <>
            <Button onClick={onOpen}>
                <i className="user delete icon"></i>
                <label className="icon-delete">Eliminar</label>
            </Button>
            <Confirm
                style={{ bottom: '40%' }}
                open={abierto}
                content='Esta seguro? Se eliminará el empleado permanentemente '
                cancelButton='Cancelar'
                confirmButton="Confirmar"
                onCancel={onClose}
                onConfirm={() => deleteEmployees(employeeId, employeeEmail, companyId)}
            />
        </>
    );
}

export default Eliminar;