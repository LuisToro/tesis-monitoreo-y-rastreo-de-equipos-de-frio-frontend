import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon, Segment, Loader, Dimmer } from 'semantic-ui-react';
import EmployeesTable from './employeesTable'

const getEmployeesByCompany = `${process.env.REACT_APP_CLIENTS_local}employeesss/`;

export class EmployeesModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            api: [],
            Messages: Array(0),
        };
    }

    getEmployeesByCompany() {
        fetch(`${getEmployeesByCompany}${this.props.companyId}`)
            .then(res => {
                return res.json()
            })
            .then(res => {
                let dat = res;
                this.setState({
                    api: dat.data,
                    foundRows: dat.data
                });
            })
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                closeIcon
                trigger={
                    <Button onClick={() => (this.getEmployeesByCompany())}>
                        <Icon name="clipboard outline"></Icon>
                        <label className="icon-text">Ver Empleados</label>
                    </Button>}
            >
                {
                    this.state.api ?
                        <Fragment>
                            <Modal.Header>
                                <Grid columns='equal'>
                                    <Grid.Column width={11}>
                                        <h1>Registro de empleados</h1>
                                    </Grid.Column>
                                </Grid>
                            </Modal.Header>

                            <Modal.Content scrolling>
                                <EmployeesTable employees={this.state.api} companyId={this.props.companyId} />
                            </Modal.Content>

                            <Modal.Actions>
                            </Modal.Actions>

                        </Fragment>
                        : <Segment placeholder>
                            <Dimmer active inverted>
                                <Loader inverted>No tiene Empleados registrados, registra algunos</Loader>
                            </Dimmer>
                        </Segment>
                }
            </Modal>
        )
    }
}

export default EmployeesModal