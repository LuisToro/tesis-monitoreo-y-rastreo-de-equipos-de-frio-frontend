import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon, Segment, Loader, Dimmer } from 'semantic-ui-react';
import InformationAndMap from './InformationAndMap'

const getMessages = `${process.env.REACT_APP_CLIENTS_local}sms/`;

export class MessagesOfANumber extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            api: [],
            Messages: Array(0),
        };
    }

    getLastMessages() {
        fetch(`${getMessages}${this.props.cellphone}`)
            .then(res => {
                return res.json()
            })
            .then(res => {
                let dat = res;
                this.setState({
                    api: dat.data,
                });
            })
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                closeIcon
                trigger={
                    <Button onClick={() => (this.getLastMessages())}>
                        <Icon name="clipboard outline"></Icon>
                        <label className="icon-text">Ver Registro</label>
                    </Button>}
            >
                {
                    this.state.api ?
                        <Fragment>
                            <Modal.Header>
                                <Grid columns='equal'>
                                    <Grid.Column width={11}>
                                        <h1>Registro de ultimas 20 localizaciones</h1>
                                    </Grid.Column>
                                </Grid>
                            </Modal.Header>

                            <Modal.Content scrolling>
                                <InformationAndMap Messages={this.state.api} />
                            </Modal.Content>

                            <Modal.Actions>
                            </Modal.Actions>

                        </Fragment>
                        : <Segment placeholder>
                            <Dimmer active inverted>
                                <Loader inverted>Cargando...</Loader>
                            </Dimmer>
                        </Segment>
                }
            </Modal>
        )
    }
}

export default MessagesOfANumber