import React, { Component } from 'react';
import { Button, Modal, Table, Icon, Label, Grid } from 'semantic-ui-react';
import GoogleMaps from '../Location/showGoogleMap'
import '../../public/style/GoogleMap.css'

const options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

export class InformationAndMap extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            address: "",
            address2: Array(0),
            xww: Array(0),
            latitude: "",
            length: "",
            tam: 0
        };
    }

    openModal = (estado) => {
        this.setState({
            open: estado,
        });
    }

    closeModal = (estado) => {
        this.setState({
            open: estado,
            latitude: "",
            length: ""
        });
    }
    getLatitude = (message) => {
        const regex = /:/g;
        const splitMessage = message.split("\n", 2);
        const latitude = regex[Symbol.split](splitMessage[1]);

        return latitude[1].split("", 1) === "N" || latitude[1].split("", 1) === "S" ?
            latitude[1].substring(1) : latitude[1].replace(latitude[1].split("", 1), "-");
    }
    getLength = (message) => {
        const regex = /:/g;
        const splitMessage = message.split("\n", 3);
        const length = regex[Symbol.split](splitMessage[2]);

        const result = (length[1].split("", 1) === "E" || length[1].split("", 1) === "O" ?
            length[1].substring(1) : length[1].replace(length[1].split("", 1), "-")).slice(0, -5);
        return result
    }

    render() {
        return (
            <div >
                <Table celled className="tarjeta-tabla">
                    <Table.Header>
                        <Table.Row >
                            <Table.HeaderCell className="cabeceras-tabla">Fecha</Table.HeaderCell>
                            <Table.HeaderCell className="cabeceras-tabla">Hora</Table.HeaderCell>
                            <Table.HeaderCell className="cabeceras-tabla">Ubicacion</Table.HeaderCell>
                            <Table.HeaderCell className="cabeceras-tabla">Acción</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>

                        {this.props.Messages.map((value) => (
                            <Table.Row key={value.sid}>
                                <Table.Cell className="bordes-tabla">
                                    <Label className="nombre">{new Date(value.dateSent).toLocaleDateString('es-BO', options)}</Label>
                                </Table.Cell >

                                <Table.Cell className="bordes-tabla">
                                    <Label className="nombre">{new Date(value.dateSent).toLocaleTimeString('es-BO', { timeStyle: 'short' })}</Label>
                                </Table.Cell>

                                <Table.Cell className="bordes-tabla">
                                    {value.address ?
                                        <>
                                            <Label>{value.address.road}, {value.address.neighbourhood} , {value.address.county}</Label>
                                        </>
                                        : <Label>-</Label>
                                    }
                                </Table.Cell>

                                <Table.Cell className="bordes-tabla">
                                    <Modal
                                        open={this.state.open}
                                        onClose={() => this.closeModal(false)}
                                        onOpen={() => this.openModal(true)}
                                        size="fullscreen"
                                        className="location-modal"
                                        closeIcon
                                        trigger={
                                            <Button onClick={() => (this.setState({ latitude: this.getLatitude(value.body), length: this.getLength(value.body) }))}>
                                                <label className="icon-text"><Icon name="map marker alternate" />Ubicacion:</label>
                                            </Button>}
                                    >
                                        <Modal.Header>
                                            <Grid columns='equal'>
                                                <Grid.Column width={11}>
                                                    <h1>Ubicacion del dispositivo</h1>
                                                </Grid.Column>
                                            </Grid>
                                        </Modal.Header>
                                        <Modal.Content >
                                            <Grid content="center">
                                                <Grid.Row>
                                                    <Grid.Column className="map">
                                                        <GoogleMaps latitud={this.state.latitude} longitud={this.state.length} />
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </Modal.Content>

                                    </Modal>

                                </Table.Cell>
                            </Table.Row>
                        ))
                        }
                    </Table.Body >
                </Table >
            </div >
        )
    }
}

export default InformationAndMap