import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon, Segment, Loader, Dimmer } from 'semantic-ui-react';
import axios from "axios";
import GoogleMaps from './showGoogleMap'
import '../../public/style/GoogleMap.css';

const getLatAndLng = `${process.env.REACT_APP_CLIENTS_local}sms/`;

var year = "";
var month = "";
var day = "";
var hour = "";
var minute = "";
var second = "";
var contadorDeIntervalo

export class getLocation extends Component {
    constructor() {
        super();
        this.state = {
            location: "empty",
            open: false,
            success: false,
        }
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    };

    closeModal(estado, contadorDeIntervalo) {
        this.StopInterval(contadorDeIntervalo);
        this.setState({
            open: estado,
            success: false,
            location: "empty"
        });
    };

    requestLocation = (cellPhone) => {
        fetch(`${getLatAndLng}${cellPhone}`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
        }).then(res => {
            const date = new Date();
            this.setState({
                exito: true,
            });
            year = date.getUTCFullYear()
            month = date.getUTCMonth()
            day = date.getUTCDate()
            hour = date.getUTCHours()
            minute = date.getUTCMinutes()
            second = date.getUTCSeconds()

            res.json().then(json => console.log(json), this.isLocationEmptyInterval())
        })
            .catch(err => {
                this.setState({ exito: false });
                console.log("error al enviar el mensaje " + err)
            })
    }

    getLocation = () => {
        axios
            .get(`
            ${getLatAndLng}
            ${this.props.cellphone}
            /${year}
            /${month}
            /${day}
            /${hour}
            /${minute}
            /${second}
            `)
            .then(response => {
                this.setState({
                    location: response.data.data,
                });
            })
            .catch(function (error) {
                console.log("Todavia no se recibió la ubicación");
            });
    };

    StopInterval = (contadorDeIntervalo) => {
        clearInterval(contadorDeIntervalo)
    }

    isLocationEmptyInterval = () => {
        contadorDeIntervalo = setInterval(() => {
            if (this.state.location === "empty") {
                this.getLocation();

            } else {
                this.StopInterval(contadorDeIntervalo);
                this.setState({ success: true });
            }
        }, 15000);
    }

    getLatitude = (message) => {
        const regex = /:/g;
        const splitMessage = message.split("\n", 2);
        const latitude = regex[Symbol.split](splitMessage[1]);

        return latitude[1].split("",1) === "N" || latitude[1].split("",1) === "S" ?   
        latitude[1].substring(1) : latitude[1].replace(latitude[1].split("",1),"-");
    }

    getLength = (message) => {
        const regex = /:/g;
        const splitMessage = message.split("\n", 3);
        const length = regex[Symbol.split](splitMessage[2]);

        const result = (length[1].split("",1) === "E" || length[1].split("",1) === "O" ? 
        length[1].substring(1) : length[1].replace(length[1].split("",1),"-")).slice(0,-5);
        return result
    }

    render() {
        return (
            <Modal
                open={this.props.open}
                onClose={() => this.closeModal(false, contadorDeIntervalo)}
                onOpen={() => this.openModal(true)}
                closeIcon
                className="location-modal"
                size="fullscreen"
                trigger={
                     <Button onClick={() => (this.requestLocation(this.props.cellphone))}>
                        <label className="icon-text"><Icon name="map marker alternate" />Direccion</label>
                    </Button>
                }
            >
                {
                    this.state.location !== "empty" ?
                        <Fragment>
                            <Modal.Header>
                                <Grid columns='equal'>
                                    <Grid.Column width={11}>
                                        <h1>Ubicacion del dispositivo</h1>
                                    </Grid.Column>
                                </Grid>
                            </Modal.Header>

                            <Modal.Content>
                                <Grid Content="center">
                                    <Grid.Row>
                                        <Grid.Column className="map">
                                            <GoogleMaps latitud={this.getLatitude(this.state.location)} longitud={this.getLength(this.state.location)} />
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Modal.Content>

                        </Fragment>
                        : <Segment placeholder size="big">
                            <Dimmer active inverted>
                                <Loader inverted>Porfavor espere mientras se solicita la ubicacion...</Loader>
                            </Dimmer>
                        </Segment>
                }
            </Modal>
        )

    }
}

export default getLocation;