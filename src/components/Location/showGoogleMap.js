import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import '../../public/style/GoogleMap.css';

const apiKey = `${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}`;

export class showGoogleMap extends Component {
    constructor() {
        super();
        this.state = {
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {}
        }
    }

    onMarkerClick = (props, marker, e) =>
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });

    onClose = props => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
    };

    render() {
        return (
            <Map
                google={this.props.google}
                zoom={17}
                initialCenter={
                    {
                        lat: this.props.latitud,
                        lng: this.props.longitud
                    }
                }
            >
                <Marker
                    onClick={this.onMarkerClick}
                />
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: apiKey
})(showGoogleMap);