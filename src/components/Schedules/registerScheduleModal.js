import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon } from 'semantic-ui-react';
import RegisterSchedule from './registerSchedule'

export class RegisterScheduleModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
        this.openModal = this.openModal.bind(this);
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                style={{ bottom: '32%'}}
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                size="tiny"
                closeIcon
                trigger={
                    <Button className={"register-schedule-button"}>
                        <Icon name="calendar plus outline"></Icon>
                        <label className="icon-text">Programar Consulta</label>
                    </Button>}
            >
                <Fragment >
                    <Modal.Header>
                        <Grid  columns='equal'>
                            <Grid.Column width={15}>
                                <h1>Fecha y Hora de la consulta</h1>
                            </Grid.Column>
                        </Grid>
                    </Modal.Header>
                    
                    <Modal.Content style={{ bottom: '40%'}} >
                        <Grid columns='equal'>
                            <Grid.Row>
                                <Grid.Column >
                                    <RegisterSchedule clientId={this.props.clientId} schedules={this.props.schedules} closeModal={this.openModal} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Content>
                </Fragment>
            </Modal>
        )
    }
}

export default RegisterScheduleModal