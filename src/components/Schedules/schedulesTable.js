import React, { Component } from 'react';
import { Label, Table } from 'semantic-ui-react';
import '../../public/style/Table.css';
import DeleteSchedule from './deleteSchedule';
import EditScheduleModal from './editScheduleModal';

export class EmployeesTable extends Component {
  constructor() {
    super();
    this.state = {
      daysSpanish:["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"],
      days:["monday","tuesday","wednesday","thursday","friday","saturday","sunday"],
      mensajeDeEstado: "",
      mostrarMensajeDeEstado: false,
      open: false
    }
  }
  translateToSpanish = (day) =>{
    for (let index = 0; index < this.state.days.length; index++) {
      if (day === this.state.days[index]) {
        return this.state.daysSpanish[index];
      }
    }
  }

  render() {
    return (
      <div>
        <div className="tabla-schedule">
          <Table celled className="tarjeta-tabla">

            <Table.Header>
              <Table.Row >
                <Table.HeaderCell className="cabeceras-tabla">Dia</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Hora</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Acción</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
                  {this.props.schedules.map((value) => (
                    <Table.Row key={value.id} >

                      <Table.Cell className="bordes-tabla">
                        <Label className="nombre"> {this.translateToSpanish(value.dia)}</Label><br></br>
                      </Table.Cell >

                      <Table.Cell className="bordes-tabla">
                        <Label> {value.hora}:{value.minuto}</Label>
                      </Table.Cell>

                      <Table.Cell colSpan="3" className="bordes-tabla">
                        <DeleteSchedule scheduleId={value.id} clientId={this.props.clientId} />
                        <EditScheduleModal schedule={value} />
                      </Table.Cell>
                    </Table.Row>
                  ))}

            </Table.Body>

            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan='4' className="no-border">
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>

        </div>
      </div >)

  }

}
export default EmployeesTable