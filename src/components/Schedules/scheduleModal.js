import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon, Segment, Loader, Dimmer } from 'semantic-ui-react';
import SchedulesTable from './schedulesTable'
import RegisterScheduleModal from './registerScheduleModal';

const getSchedulesByClient = `${process.env.REACT_APP_CLIENTS_local}schedule/`;

export class SchedulesModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            api: [],
            Messages: Array(0),
        };
    }

    getSchedulesByClient() {
        fetch(`${getSchedulesByClient}${this.props.clientId}`)
            .then(res => {
                return res.json()
            })
            .then(res => {
                let dat = res;
                this.setState({
                    api: dat.data,
                });
            })
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                closeIcon
                trigger={
                    <Button onClick={() => (this.getSchedulesByClient())}>
                        <Icon name="calendar alternate outline"></Icon>
                        <label className="icon-text">Ver consultas</label>
                    </Button>}
            >
                <Modal.Header>
                    <Grid columns='equal'>
                        <Grid.Column width={11}>
                            <h1>Registro de consultas programadas</h1>
                        </Grid.Column>
                    </Grid>
                </Modal.Header>
                <div className="tabla-menu">
                    <RegisterScheduleModal clientId={this.props.clientId} schedules={this.state.api}/>
                </div><br /><br /><br />
                {
                    this.state.api ?
                        <Fragment>
                            <Modal.Content scrolling>
                                <SchedulesTable schedules={this.state.api} clientId={this.props.clientId} />
                            </Modal.Content>
                        </Fragment>

                        : <Segment placeholder size="big">
                            <Dimmer active inverted>
                                <Loader inverted>No tiene consultas registradas, registra algunas</Loader>
                            </Dimmer>
                        </Segment>
                }
            </Modal>
        )
    }
}

export default SchedulesModal