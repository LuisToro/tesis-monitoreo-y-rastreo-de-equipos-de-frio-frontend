import React, { Component } from 'react';
import { Button, Grid, GridRow, Confirm } from 'semantic-ui-react'
import { Form, Dropdown } from 'semantic-ui-react-form-validator'
import { TimeInput } from 'semantic-ui-react-datetimeinput';
import '../../public/style/registerSchedule.css';

const theDate = new Date();
const options = [
    {
        key: 'monday',
        text: 'LUNES',
        value: 'monday',
    },
    {
        key: 'tuesday',
        text: 'MARTES',
        value: 'tuesday',
    },
    {
        key: 'wednesday',
        text: 'MIERCOLES',
        value: 'wednesday',
    },
    {
        key: 'thursday',
        text: 'JUEVES',
        value: 'thursday',
    },
    {
        key: 'friday',
        text: 'VIERNES',
        value: 'friday',
    },
    {
        key: 'saturday',
        text: 'SABADO',
        value: 'saturday',
    },
    {
        key: 'sunday',
        text: 'DOMINGO',
        value: 'sunday',
    }
];
const edit = `${process.env.REACT_APP_CLIENTS_local}schedule`;
export class RegisterSchedule extends Component {
    state = {
        success: null
    };
    constructor(props) {
        super(props);
        this.state = {
            minuto: this.props.schedule.minuto,
            hora: this.props.schedule.hora,
            dropdown: this.props.schedule.dia,
            dateValue: theDate.setHours(this.props.schedule.hora, this.props.schedule.minuto),
            open: false,
            exito: null
        }
        this.closeTheModal = this.closeTheModal.bind(this);
    }

    closeTheModal() {
        this.props.closeModal(this.state.open)
    }

    state = {}
    handleChange = (e, { value }) => {
        this.setState({ value })
        this.setState({ dropdown: value })
    }

    changeDateValue = (newDateValue) => {
        this.setState({ dateValue: newDateValue });
        this.setState({ minuto: newDateValue.getMinutes() });
        this.setState({ hora: newDateValue.getHours() });
    }

    onChange = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    }

    register = () => {
        var schedule = {
            minuto: this.state.minuto,
            hora: this.state.hora,
            dia: this.state.dropdown,
            ClientId: this.state.clientId
        }
        fetch(`${edit}/${this.props.schedule.id}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(schedule)
        }).then(res => {
            this.setState({ exito: true });
            res.json()
                .then(json => console.log(json))
            window.open("/", "_self");
        })
            .catch(err => {
                this.setState({ exito: false });
                console.log("error al leer los datos " + err)
            })
    }

    handleCancel = () => this.setState({ open: false })
    areTheValuesCorrect(e) {

        e.preventDefault();
        if (typeof (this.state.dropdown) !== undefined) {
            this.setState({ open: true, minuto: new Date(this.state.dateValue).getMinutes(), hora: new Date(this.state.dateValue).getHours() })
        } else {
            this.setState({ open: false })
        }
    }
    render() {
        return (
            <div className="backform">
                <Grid.Column className="time-pick">
                    <TimeInput size={"big"}
                        buttonPlacement="buttonsInside"
                        dateValue={new Date(this.state.dateValue)}
                        onDateValueChange={this.changeDateValue}
                    />
                </Grid.Column>
                <Form id="myForm" className="ui form" onSubmit={this.areTheValuesCorrect.bind(this)}>
                    <Grid columns={1}>
                        <Grid.Column>
                        <Dropdown
                                label="Dia de la semana"
                                placeholder="Selecciona el dia"
                                onChange={this.handleChange}
                                value={this.state.dropdown}
                                validators={['required']}
                                errorMessages={['this field is required']}
                                options={options}
                            />
                        </Grid.Column>
                    </Grid>
                    <Grid centered rows={2} columns={2}>
                        <GridRow>
                            <Button className="ui basic negative button" onClick={this.closeTheModal} >Cancelar</Button>
                            <Confirm
                                style={{ bottom: '40%' }}
                                header='¿Está seguro que desea guardar los cambios?'
                                content="Si confirma el guardado, será redirigido a la lista principal"
                                open={this.state.open}
                                cancelButton='Cancelar'
                                confirmButton='Confirmar'
                                onCancel={this.handleCancel}
                                onConfirm={this.register}
                            />
                            <Button className="ui basic positive button">Confirmar</Button>
                        </GridRow>
                    </Grid>
                </Form>
            </div>
        )
    }
}


export default RegisterSchedule