import React from "react";
import { Button, Confirm } from "semantic-ui-react";
import axios from "axios";

const deleteSchedules = `${process.env.REACT_APP_CLIENTS_local}schedule/`;

function Eliminar({ scheduleId, clientId }) {
    const [abierto, setOpen] = React.useState(false);

    const onOpen = () => setOpen(true);
    const onClose = () => setOpen(false);

    const deleteSchedule = (scheduleId, clientId) => {
        axios
            .delete(`${deleteSchedules}${scheduleId}/client/${clientId}`)
            .then(response => {
                window.open("/", "_self");
            })
            .catch(function (error) {
                console.log(error);
            });
        onClose();
    }

    return (
        <>
            <Button onClick={onOpen}>
                <i className="calendar times outline icon"></i>
                <label className="icon-delete">Eliminar</label>
            </Button>
            <Confirm
                style={{ bottom: '40%' }}
                open={abierto}
                content='Esta seguro? Se eliminará la consulta programada permanentemente '
                cancelButton='Cancelar'
                confirmButton="Confirmar"
                onCancel={onClose}
                onConfirm={() => deleteSchedule(scheduleId, clientId)}
            />
        </>
    );
}

export default Eliminar;