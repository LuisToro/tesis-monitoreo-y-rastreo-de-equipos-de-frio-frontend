import React, { Component, Fragment } from 'react';
import axios from "axios";
import { Button, Modal, Grid, Icon } from 'semantic-ui-react';
import '../../public/style/registerClient.css';
import RegisterClient from './registerClient'

const getCompany = `${process.env.REACT_APP_CLIENTS_local}employeess/`;
export class RegisterClientModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            success: true,
            visible: true
        };
        this.openModal = this.openModal.bind(this);
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    isTheDataCorrect(correct) {
        if (correct === true) {

        } else {

        }
        this.setState({
            success: correct
        });
    }

    getCompanyId(email) {
        axios
            .get(`${getCompany}${email}`)
            .then(response => {
                this.setState({
                    companyId: response.data.data
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                size="tiny"
                className="register-client"
                closeIcon
                trigger={
                    <Button className="register-button" onClick={() => (this.getCompanyId(this.props.userEmail))}>
                        <Icon name="address book outline"></Icon>
                        <label className="icon-text">Registrar Cliente</label>
                    </Button>}
            >
                <Fragment>
                    <Modal.Header>
                        <Grid columns='equal'>
                            <Grid.Column>
                                <h1>Registra a un empleado</h1>
                            </Grid.Column>
                        </Grid>
                    </Modal.Header>

                    <Modal.Content >
                        <Grid content="center" columns='equal'>
                            <Grid.Row >
                                <Grid.Column >
                                    <RegisterClient companyId={this.state.companyId} closeModal={this.openModal} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Content>
                </Fragment>
            </Modal>
        )
    }
}

export default RegisterClientModal