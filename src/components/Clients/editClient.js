import React, { Component } from 'react';
import { Button, Grid, GridRow, Confirm } from 'semantic-ui-react'
import { Form, Input, TextArea } from 'semantic-ui-react-form-validator'
import '../../public/style/registerClient.css';

const editClient = `${process.env.REACT_APP_CLIENTS_local}clients/`;

export class EditClient extends Component {
    state = {
        success: null
    };
    constructor(props) {
        super(props);
        this.state = {
            nombre: this.props.client.nombre,
            numero: this.props.client.numero,
            codigoContrato: this.props.client.codigoContrato,
            direccion: this.props.client.direccion,
            detalle: this.props.client.detalle,
            equipoFrio: this.props.client.equipoFrio,
            descripcionEquipoFrio: this.props.client.descripcionEquipoFrio,
            celularEquipoFrio: this.props.client.celularEquipoFrio,
            open: false,
            exito: null
        }
        this.closeTheModal = this.closeTheModal.bind(this);
    }
 
    closeTheModal() {
       this.props.closeModal(this.state.open)
    }

    onChange = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    }

    register = () => {
        var client = {
            nombre: this.state.nombre,
            numero: this.state.numero,
            codigoContrato: this.state.codigoContrato,
            direccion: this.state.direccion,
            detalle: this.state.detalle,
            equipoFrio: this.state.equipoFrio,
            descripcionEquipoFrio: this.state.descripcionEquipoFrio,
            celularEquipoFrio: this.state.celularEquipoFrio,
        }
        fetch(`${editClient}${this.props.client.id}`,{
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(client)
        }).then(res => {
            this.setState({ exito: true });
            res.json()
                .then(json => console.log(json))
            window.open("/", "_self");
        })
            .catch(err => {
                this.setState({ exito: false });
                console.log("error al leer los datos " + err)
            })
    }

    handleCancel = () => this.setState({ open: false })
    areTheValuesCorrect(e) {
        e.preventDefault();
  
        if (typeof (this.state.nombre) !== undefined ||
           typeof (this.state.numero) !== undefined ||
           typeof (this.state.celularEquipoFrio) !== undefined) {
           this.setState({ open: true })
        } else {
           this.setState({ open: false })
        }
     }

    render() {
        return (
            <div className="backform">
                <Form id="myForm" className="ui form" onSubmit={this.areTheValuesCorrect.bind(this)}>
                    <Grid columns={2} >
                        <Grid.Row>
                            <Grid.Column>
                                <Input
                                    placeholder="Nombre"
                                    label='Nombre del cliente'
                                    validators={['required', 'matchRegexp:^[A-Za-z_ ]+$']}
                                    errorMessages={['Este campo es requerido', 'El campo no acepta valores numéricos']}
                                    onChange={this.onChange}
                                    value={this.state.nombre}
                                    maxLength="40"
                                    type="text"
                                    name="nombre"
                                />
                                <Input
                                    label='Numero de telefono'
                                    placeholder="Numero de telefono"
                                    value={this.state.numero}
                                    validators={['required', 'matchRegexp:^[0-9]+$']}
                                    errorMessages={['Este campo es requerido', 'El campo sólo acepta números']}
                                    onChange={this.onChange}
                                    type="text"
                                    name="numero"
                                />
                                <Input
                                    type="text"
                                    name="direccion"
                                    label='Direccion'
                                    placeholder="Direccion"
                                    value={this.state.direccion}
                                    onChange={this.onChange}
                                />
                                <TextArea
                                    className="text-area"
                                    type="text"
                                    name="detalle"
                                    label='Detalle del cliente'
                                    placeholder='Detalle del cliente'
                                    value={this.state.detalle}
                                    onChange={this.onChange}
                                />
                            </Grid.Column>
                            <Grid.Column>
                                <Input
                                    type="text"
                                    name="codigoContrato"
                                    label='Codigo de contrato'
                                    placeholder="Codigo de Contrato"
                                    value={this.state.codigoContrato}
                                    validators={['required']}
                                    errorMessages={['Este campo es requerido']}
                                    onChange={this.onChange}
                                />
                                <Input
                                    label='Numero de telefono del equipo de frio'
                                    placeholder="Numero de telefono"
                                    value={this.state.celularEquipoFrio}
                                    validators={['matchRegexp:^[0-9]+$']}
                                    errorMessages={['El campo sólo acepta números']}
                                    onChange={this.onChange}
                                    type="text"
                                    name="celularEquipoFrio"
                                />
                                <Input
                                    type="text"
                                    name="equipoFrio"
                                    label='Modelo del quipo de frio'
                                    placeholder="Modelo del Equipo de frio"
                                    value={this.state.equipoFrio}
                                    onChange={this.onChange}
                                />
                                <TextArea
                                    className="text-area"
                                    type="text"
                                    name="descripcionEquipoFrio"
                                    label='Descripcion del equipo de frio'
                                    placeholder='Descripcion'
                                    value={this.state.descripcionEquipoFrio}
                                    onChange={this.onChange}
                                />

                            </Grid.Column>
                        </Grid.Row>
                    </Grid>

                    <Grid centered rows={2} columns={2}>
                        <GridRow>
                            <Button className="ui basic negative button" onClick={this.closeTheModal}>Cancelar</Button>
                            <Confirm
                                style={{ bottom: '40%'}}
                                header='¿Está seguro que desea guardar los cambios?'
                                content="Si confirma el guardado, será redirigido a la lista principal"
                                open={this.state.open}
                                cancelButton='Cancelar'
                                confirmButton='Confirmar'
                                onCancel={this.handleCancel}
                                onConfirm={this.register}
                            />
                            <Button className="ui basic positive button">Confirmar</Button>
                        </GridRow>
                    </Grid>
                </Form>
            </div>
        )
    }
}

export default EditClient;