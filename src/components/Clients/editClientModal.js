import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon } from 'semantic-ui-react';
import '../../public/style/registerClient.css';
import EditClient from './editClient'

export class EditClientModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
        this.openModal = this.openModal.bind(this);
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                size="tiny"
                className="register-client"
                closeIcon
                trigger={
                    <Button className="edit-button">
                        <Icon name="edit outline"></Icon>
                        <label className="icon-text">Editar Cliente</label>
                    </Button>}
            >
                <Fragment>
                    <Modal.Header>
                        <Grid columns='equal'>
                            <Grid.Column>
                                <h1>Edita a un cliente</h1>
                            </Grid.Column>
                        </Grid>
                    </Modal.Header>

                    <Modal.Content>
                        <Grid content="center" columns='equal'>
                            <Grid.Row >
                                <Grid.Column >
                                    <EditClient client={this.props.client} closeModal={this.openModal}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Content>
                </Fragment>
            </Modal>
        )
    }
}

export default EditClientModal