import React from "react";
import { Form, Input, Icon, Item } from "semantic-ui-react";
import '../../../public/style/clientInformation.css';

function ClientInformation({ client }) {
  return (
    <Item.Group>
      <Item>
        <Item.Content verticalAlign='middle'>

          <Item.Description>
            <Form id="form-data">

              <Form.Field inline>
                <label><Icon name='user outline' /><span >Nombre:</span></label>
                <Input className="information-text"> {client.nombre} </Input>
              </Form.Field>

              <Form.Field inline>
                <label className="icon-text"><Icon name='map outline' />Direccion:</label>
                <Input className="information-text"> {client.direccion} </Input>
              </Form.Field>

              <Form.Field inline>
                <label><Icon name='call square' /><span >Telefono:</span></label>
                <Input className="information-text"> {client.numero} </Input>
              </Form.Field>

              <Form.Field inline>
                <label><Icon name='file alternate outline' /><span >Detalle del cliente:</span></label>
                <Input className="description-text"> {client.detalle} </Input>
              </Form.Field>

              <Form.Field inline>
                <label><Icon name='box' /><span >Equipo de frio:</span></label>
                <Input className="information-text"> {client.equipoFrio} </Input>
              </Form.Field>

              <Form.Field inline>
                <label><Icon name='file alternate outline' /><span > Descripcion del equipo de frio:</span></label>
                <Input className="description-text"> {client.descripcionEquipoFrio} </Input>
              </Form.Field>

            </Form>
          </Item.Description>
        </Item.Content>
      </Item>
    </Item.Group>
  );
}

export default ClientInformation;
