import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon, Segment, Loader, Dimmer } from 'semantic-ui-react';
import axios from "axios";
import ClientInformation from './clientInformation'
import GetLocation from '../../Location/getLocation';
import EditClientModal from '../editClientModal';

const getClient = `${process.env.REACT_APP_CLIENTS_local}clients/`;

export class ClientModal extends Component {
  constructor() {
    super();
    this.state = {
      open: false
    };
  }

  getAClient() {
    axios
      .get(`${getClient}${this.props.clientId}`)
      .then(response => {
        this.setState({
          client: response.data.data
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  openModal(estado) {
    this.setState({
      open: estado
    });
  }

  render() {
    return (
      <Modal
        open={this.state.open}
        onClose={() => this.openModal(false)}
        onOpen={() => this.openModal(true)}
        size="tiny"
        closeIcon
        trigger={
          <Button onClick={() => (this.getAClient(this.props.clientId))}>
            <Icon name="address card outline"></Icon>
            <label className="icon-text">Ver Informacion del Cliente</label>
          </Button>}
      >
        {
          this.state.client ?
            <Fragment>
              <Modal.Header>
                <Grid columns='equal'>
                  <Grid.Column width={11}>
                    <h1>Informacion del cliente</h1>
                  </Grid.Column>
                </Grid>
              </Modal.Header>

              <Modal.Content>
                <Grid columns='equal'>
                  <Grid.Row>
                    <Grid.Column >
                      <ClientInformation client={this.state.client} />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Modal.Content>
              <Modal.Actions>
                <EditClientModal client={this.state.client}/>
                <GetLocation cellphone={this.state.client.celularEquipoFrio} />
              </Modal.Actions>

            </Fragment>
            : <Segment placeholder>
              <Dimmer active inverted>
                <Loader inverted>Cargando...</Loader>
              </Dimmer>
            </Segment>
        }
      </Modal>
    )
  }
}

export default ClientModal