import React from "react";
import { Button, Confirm } from "semantic-ui-react";
import axios from "axios";

const deleteClient = `${process.env.REACT_APP_CLIENTS_local}clients/`;

function Eliminar({ clientId, updateList }) {
    const [abierto, setOpen] = React.useState(false);

    const onOpen = () => setOpen(true);
    const onClose = () => setOpen(false);

    const deleteClientFromDB = (clientId) => {
        axios
            .delete(`${deleteClient}${clientId}`)
            .then(response => {
                updateList();
            })
            .catch(function (error) {
                console.log(error);
            });
        onClose();
    }

    return (
        <>
            <Button onClick={onOpen}>
                <i className="user delete icon"></i>
                <label className="icon-delete">Eliminar</label>
            </Button>
            <Confirm
                open={abierto}
                content='El cliente se eliminará permanentemente'
                cancelButton='Cancelar'
                confirmButton="Confirmar"
                onCancel={onClose}
                onConfirm={() => deleteClientFromDB(clientId)}
            />
        </>
    );
}

export default Eliminar;