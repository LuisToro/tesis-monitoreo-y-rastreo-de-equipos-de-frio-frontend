import React, { Component } from 'react';
import { Label, Message, Segment, Table, Search, Loader, Dimmer } from 'semantic-ui-react';
import '../../public/style/Table.css';
import DeleteClient from './deleteClient';
import ClientModal from '../Clients/Show-A-Client/clientModal';
import LastMessagesModal from '../Sms/lastMessagesModal';
import ScheduleModal from '../Schedules/scheduleModal'
import RegisterClient from './registerClientModal'
import EditEmployeePasswordModal from '../Employees/employeeEditPasswordModal'

import { withAuth0 } from '@auth0/auth0-react';

const showClients = `${process.env.REACT_APP_CLIENTS_local}clientss`;
const employeePaswoord = `${process.env.REACT_APP_CLIENTS_local}employees`;

export class ClientsTable extends Component {
  constructor() {
    super();
    this.state = {
      api: [],
      schedule: [],
      foundRows: Array(0),
      isTheFirstPassword: "",
      open: false
    }
  }
  getEmployeeEmail() {
    const { user } = this.props.auth0;
    return user.email
  }

  getClients() {
    fetch(`${showClients}/${this.getEmployeeEmail()}`)
      .then(res => {
        return res.json()
      })
      .then(res => {
        let dat = res;
        this.setState({
          api: dat.data,
          foundRows: dat.data
        });
      })
  }
  getEmployeePassword() {
    fetch(`${employeePaswoord}/${this.getEmployeeEmail()}`)
      .then(res => {
        return res.json()
      })
      .then(res => {
        this.setState({
          isTheFirstPassword: res.data.password
        });
      })
  }

  componentDidMount() {
    this.getClients();
    this.getEmployeePassword();
  }

  findByName(name) {
    let wanted = name.target.value;
    let clients = this.state.api;
    let result = Array(0)
    if (name.target.value.trim() === "") {
      this.setState({
        foundRows: this.state.api
      });
    }
    for (let count = 0; count < clients.length; count++) {
      if (clients[count].nombre.toLowerCase().includes(wanted.toLowerCase())) {
        result.push(clients[count]);
      }
    }
    this.setState({
      foundRows: result
    });
  }

  render() {
    return (
      <div>
        <div style={{ textAlign: 'center' }}>
          {this.state.isTheFirstPassword === 1 ?
            <Message
              warning
              floating
              compact
              size='large'
              header='Cambio de contraseña'
              content='Para una mayor seguridad se le sugiere cambiar la contraseña predeterminada'
            ></Message>
            : <p></p>
          }
        </div>
        <div className="tabla">
          <EditEmployeePasswordModal email={this.getEmployeeEmail()} />
          <p className="titulo">Lista de Clientes</p>
          <div className="linea"></div>
          <div className="tabla-menu">
            <Search
              showNoResults={false}
              onSearchChange={this.findByName.bind(this)}
              style={{ width: "auto" }}
            >
            </Search>
            <RegisterClient userEmail={this.getEmployeeEmail()} />
          </div>
          <br /><br />
          <Table celled className="tarjeta-tabla">
            <Table.Header>
              <Table.Row >
                <Table.HeaderCell className="cabeceras-tabla">Nombre</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Direccion</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Telefono</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Acciónes Clientes</Table.HeaderCell>
                <Table.HeaderCell className="cabeceras-tabla">Acciónes Consultas</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {this.state.foundRows !== undefined ?
                <>
                  {this.state.foundRows.map((value) => (
                    <Table.Row key={value.id} >
                      <Table.Cell className="bordes-tabla">
                        <Label className="nombre">{value.nombre}</Label><br></br>
                      </Table.Cell >

                      <Table.Cell className="bordes-tabla">
                        <Label>• {value.direccion}</Label>
                      </Table.Cell>

                      <Table.Cell className="bordes-tabla">
                        <Label>• {value.numero}</Label>
                      </Table.Cell>

                      <Table.Cell className="bordes-tabla">
                        <ClientModal clientId={value.id} open={this.state.mostrarModal} />
                        <DeleteClient clientId={value.id} updateList={() => this.getClients()} />
                      </Table.Cell>

                      <Table.Cell className="bordes-tabla">
                        <LastMessagesModal cellphone={value.celularEquipoFrio} />
                        <ScheduleModal clientId={value.id} />
                      </Table.Cell>

                    </Table.Row>
                  ))}</>
                :
                <Table.Row className="no-users-message">
                  <Segment clearing textAlign="center" placeholder size="big">
                    <Dimmer active inverted>
                      <Loader inverted>Ups no tienes clientes registrados, porfavor registra un cliente...</Loader>
                    </Dimmer>
                  </Segment>
                </Table.Row>
              }
            </Table.Body>
            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan='4' className="no-border">
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
        </div>
      </div>
    )
  }
}
export default withAuth0(ClientsTable);