import React, { Component } from 'react';
import { Label, Table, Search, Loader, Dimmer } from 'semantic-ui-react';
import '../../public/style/Table.css';
import DeleteCompany from './deleteCompany';
import RegisterEmployeeModal from './../Employees/employeeRegisterModal';
import EmployeesModal from '../Employees/employeesModal';
import RegisterCompanyModal from './registerCompanyModal';
import EditCompanyModal from './editCompanyModal';

const showCompanies = `${process.env.REACT_APP_CLIENTS_local}company`;
const getByCompany = `${process.env.REACT_APP_CLIENTS_local}employeess/`;

export class CompaniesTable extends Component {
    constructor() {
        super();
        this.state = {
            api: [],
            employees: [],
            foundRows: Array(0),
            mensajeDeEstado: "",
            mostrarMensajeDeEstado: false,
            open: false
        }
    }

    getCompanies() {
        fetch(showCompanies)
            .then(res => {
                return res.json()
            })
            .then(res => {
                let dat = res;
                this.setState({
                    api: dat.data,
                    foundRows: dat.data
                });
            })
    }

    getEmployeesByCompany(id) {
        fetch(`${getByCompany}${id}`)
            .then(res => {
                return res.json()
            })
            .then(res => {
                let dat = res;
                this.setState({
                    employees: dat.data,
                });
            })
    }

    componentDidMount() {
        this.getCompanies();
    }

    findByName(name) {
        let wanted = name.target.value;
        let companies = this.state.api;
        let result = Array(0)
        if (name.target.value.trim() === "") {
            this.setState({
                foundRows: this.state.api
            });
        }
        for (let count = 0; count < companies.length; count++) {
            if (companies[count].nombre.toLowerCase().includes(wanted.toLowerCase())) {
                result.push(companies[count]);
            }
        }
        this.setState({
            foundRows: result
        });
    }

    render() {
        return (
            <div>
                <div className="tabla">
                    <p className="titulo">Lista de Empresas afiliadas</p>
                    <div className="linea"></div>
                    <div className="tabla-menu">
                        <Search
                            showNoResults={false}
                            onSearchChange={this.findByName.bind(this)}
                            style={{ width: "auto" }}
                        >
                        </Search>
                        <RegisterCompanyModal />
                    </div>
                    <br />
                    <br />
                    <Table celled className="tarjeta-tabla">
                        <Table.Header>
                            <Table.Row >
                                <Table.HeaderCell className="cabeceras-tabla">Nombre</Table.HeaderCell>
                                <Table.HeaderCell className="cabeceras-tabla">Acciónes Empleados</Table.HeaderCell>
                                <Table.HeaderCell className="cabeceras-tabla">Acciónes Empresas</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.state.foundRows !== undefined ?
                                <>
                                    {this.state.foundRows.map((value) => (
                                        <Table.Row key={value.id} >
                                            <Table.Cell className="bordes-tabla">
                                                <Label className="nombre">{value.nombre}</Label><br></br>
                                            </Table.Cell >

                                            <Table.Cell colSpan="0" className="bordes-tabla">
                                                <RegisterEmployeeModal companyId={value.id} open={this.state.mostrarModal} />
                                                <EmployeesModal companyId={value.id} />
                                            </Table.Cell>
                                            
                                            <Table.Cell colSpan="0" className="bordes-tabla">
                                                <EditCompanyModal company={value} />
                                                <DeleteCompany companyId={value.id} updateList={() => this.getCompanies()} />
                                            </Table.Cell>

                                        </Table.Row>
                                    ))}</>
                                :
                                <div className="no-users-message">
                                    <Dimmer active inverted>
                                        <Loader inverted>Ups no tienes clientes registrados, porfavor registra un cliente...</Loader>
                                    </Dimmer>
                                </div>

                            }
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='4' className="no-border">
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>

                </div>
            </div >)

    }

}
export default CompaniesTable