import React from "react";
import { Button, Confirm } from "semantic-ui-react";
import axios from "axios";

const deleteCompany = `${process.env.REACT_APP_CLIENTS_local}company/`;

function Eliminar({ companyId, updateList }) {
    const [abierto, setOpen] = React.useState(false);

    const onOpen = () => setOpen(true);
    const onClose = () => setOpen(false);

    const deleteCompanies = (companyId) => {
        axios
            .delete(`${deleteCompany}${companyId}`)
            .then(response => {
                updateList();
            })
            .catch(function (error) {
                console.log(error);
            });
        onClose();
    }

    return (
        <>
            <Button onClick={onOpen}>
                <i className="delete icon"></i>
                <label className="icon-delete">Eliminar Empresa</label>
            </Button>
            <Confirm
                open={abierto}
                content='Esta seguro? Se eliminará la empresa, sus empleados y sus clientes permanentemente '
                cancelButton='Cancelar'
                confirmButton="Confirmar"
                onCancel={onClose}
                onConfirm={() => deleteCompanies(companyId)}
            />
        </>
    );
}

export default Eliminar;