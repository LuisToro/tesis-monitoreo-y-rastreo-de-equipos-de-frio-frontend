import React, { Component } from 'react';
import { Button, Grid, GridRow, Confirm } from 'semantic-ui-react'
import { Form, Input } from 'semantic-ui-react-form-validator'
import '../../public/style/registerClient.css';

const edit = `${process.env.REACT_APP_CLIENTS_local}company`;

export class RegisterCompany extends Component {
   state = {
      success: null
   };
   constructor(props) {
      super(props);
      this.state = {
         nombre: this.props.company.nombre,
         open: false,
         exito: null
      }
      this.closeTheModal = this.closeTheModal.bind(this);
   }

   closeTheModal() {
      this.props.closeModal(this.state.open)
   }

   onChange = (event) => {
      let nam = event.target.name;
      let val = event.target.value;
      this.setState({ [nam]: val });
   }

   register = () => {
      var company = {
         nombre: this.state.nombre
      }
      fetch(`${edit}/${this.props.company.id}`, {
         method: 'PUT',
         headers: { 'Content-Type': 'application/json' },
         body: JSON.stringify(company)
      }).then(res => {
         this.setState({ exito: true });
         res.json()
            .then(json => console.log(json))
         window.open("/", "_self");
      })
         .catch(err => {
            this.setState({ exito: false });
            console.log("error al leer los datos " + err)
         })
   }

   handleCancel = () => this.setState({ open: false })
   areTheValuesCorrect(e) {
      e.preventDefault();
      if (typeof (this.state.nombre) !== undefined) {
          this.setState({ open: true, minuto: new Date(this.state.dateValue).getMinutes(), hora: new Date(this.state.dateValue).getHours() })
      } else {
          this.setState({ open: false })
      }
  }

   render() {
      return (
         <div className="backform">
            <Form id="myForm" className="ui form" onSubmit={this.areTheValuesCorrect.bind(this)}>
               <Grid>
                  <Grid.Column>
                     <Input
                        width="16"
                        placeholder="Nombre"
                        label='Nombre de la empresa'
                        validators={['required', 'matchRegexp:^[A-Za-z_ ]+$']}
                        errorMessages={['Este campo es requerido', 'El campo no acepta valores numéricos']}
                        onChange={this.onChange}
                        value={this.state.nombre}
                        maxLength="40"
                        type="text"
                        name="nombre"
                     />
                  </Grid.Column>
               </Grid>

               <Grid centered rows={2} columns={2}>
                  <GridRow>
                     <Button className="ui basic negative button" onClick={this.closeTheModal} >Cancelar</Button>
                     <Confirm
                        header='¿Está seguro que desea guardar los cambios?'
                        content="Si confirma el guardado, será redirigido a la lista principal"
                        open={this.state.open}
                        cancelButton='Cancelar'
                        confirmButton='Confirmar'
                        onCancel={this.handleCancel}
                        onConfirm={this.register}
                     />
                     <Button className="ui basic positive button">Confirmar</Button>
                  </GridRow>
               </Grid>
            </Form>
         </div>
      )
   }
}


export default RegisterCompany