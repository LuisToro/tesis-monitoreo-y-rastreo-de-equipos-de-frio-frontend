import React, { Component, Fragment } from 'react';
import { Button, Modal, Grid, Icon } from 'semantic-ui-react';
import EditCompany from './editCompany'

export class RegisterEmployeeModal extends Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
        this.openModal = this.openModal.bind(this);
    }

    openModal(estado) {
        this.setState({
            open: estado
        });
    }

    render() {
        return (
            <Modal
                open={this.state.open}
                onClose={() => this.openModal(false)}
                onOpen={() => this.openModal(true)}
                size="tiny"
                closeIcon
                trigger={
                    <Button >
                        <Icon name="edit outline"></Icon>
                        <label className="icon-text">Editar Empresa</label>
                    </Button>}
            >
                <Fragment>
                    <Modal.Header>
                        <Grid columns='equal'>
                            <Grid.Column width={11}>
                                <h1>Edita una Empresa</h1>
                            </Grid.Column>
                        </Grid>
                    </Modal.Header>

                    <Modal.Content>
                        <Grid columns='equal'>
                            <Grid.Row>
                                <Grid.Column >
                                    <EditCompany company={this.props.company} closeModal={this.openModal}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Content>
                </Fragment>
            </Modal>
        )
    }
}

export default RegisterEmployeeModal