import React from "react";
import AuthButton from "./auth-button";

const NavbarAuth = () => (
  <div className="navbar-nav ml-auto">
    <AuthButton />
  </div>
);

export default NavbarAuth;