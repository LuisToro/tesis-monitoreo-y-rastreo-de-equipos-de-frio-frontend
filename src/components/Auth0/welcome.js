import React, { Component } from "react";
import { Segment } from 'semantic-ui-react'
import logo from '../../public/images/frase.png';
class Welcome extends Component {

  render() {
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '15vh' }}>
          <h1>Bienvenido a la pagina de inicio del control y monitoreo de equipos de frio</h1>
        </div>
        <Segment placeholder>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <img src={logo} style={{ width: '250px' }}></img>
          </div>
        </Segment>
      </div>
    );
  };
}
export default Welcome;
