import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import { BrowserRouter as Router } from 'react-router-dom';
import Navbar from './Utils/Navbar';
import ClientsTable from './components/Clients/Table';
import CompaniesTable from './components/Companies/companyTable';
import Welcome from './components/Auth0/welcome';
import ProtectedRoute from './components/Auth0/protected-route';
import { useAuth0 } from "@auth0/auth0-react";

function App() {
  const {user, isAuthenticated: estaAutenticado } = useAuth0();
  return (
    <div>
      <Router>
        <Navbar />
        {estaAutenticado ?
          <>
            {user.sub === process.env.REACT_APP_AUTH0_ADMIN_ID ?
              <>
                <ProtectedRoute path="/" exact component={CompaniesTable} />
              </>
              : <>
                <ProtectedRoute path="/" exact component={ClientsTable} />
              </>
            }
          </>
          : <Welcome />}
      </Router>
    </div>
  );
}

export default App;
