import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../public/style/Navbar.css';
import AuthNavbar from '../components/Auth0/navbar'

export default class Navbar extends Component {
    render() {
        return (

            <div className="bar">
                <Link to={`/`}>
                    <label className="nav-titulo">Gestor de Registros</label>
                </Link>
                <div className="auth-nav"><AuthNavbar /></div>
            </div >


        )
    }
}